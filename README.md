# MediastinalLymphNodeSegmentation Algorithm

Challenge Submission of the CompAI team for the MICCAI Lymph Node Quantification Challenge. This repository contains two different model weights and the inference code given under repository tag "Official-LNQ2023-Challenge-Submission" and "Model7-Technical-Report".
For further information on those models please refer to our technical report published at MELBA journal: www.melba.com

# Challenge Setting

Accurate lymph node size estimation is critical for staging cancer patients, initial therapeutic management, and in longitudinal scans, assessing response to therapy. Current standard practice for quantifying lymph node size is based on a variety of criteria that use unidirectional or bidirectional measurements on just one or a few nodes, typically on just one axial slice. But humans have hundreds of lymph nodes, any number of which may be enlarged to various degrees due to disease or immune response. While a normal lymph node may be approximately 5 mm in diameter, a diseased lymph node may be several cm in diameter. The mediastinum, the anatomical area between the lungs and around the heart, may contain ten or more lymph nodes, often with three or more enlarged greater than 1 cm. Accurate segmentation in 3D would provide more information to evaluate lymph node disease. Full 3D segmentation of all abnormal lymph nodes in a scan promises to provide better sensitivity to detect volumetric changes indicating response to therapy. 

<br>

The LNQ2023 Challenge training dataset will consist of a unique set of high-quality pixel-level annotations of one or more clinically relevant lymph nodes in a dataset part of cancer clinical trials. The goal will be to segment all lymph nodes larger than 1 cm in diameter in the mediastinum. Participants will be provided with a subset of cases that are partially annotated (i.e. one node out of five), and evaluation of the algorithms will be performed on a distinct dataset that is fully annotated (i.e. all clinically relevant nodes).

The challenge can be accessed here: https://lnq2023.grand-challenge.org/
And more information on the Challenge Proposal can be found here: https://zenodo.org/records/7844666


## Using the model via the docker container

It is possible to use docker for inference. If you want to use the model you can use test.sh script that will build a docker container and run inference on the image at location './test/images/mediastinal-ct/*.nii.gz' (only one image per run).

We offer the model weights and inference process of our challenge submission and Model 7 of our technical report. Final Performance on the LNQ2023 test set can be found in detail in our Technical Report.

- "Model7-Technical-Report": This is the trained model and the inference code used for the best performing Model 7 of our technical Report. Performance on the LNQ2023 Test Set: Dice score=0.663, ASSD=3.97mm
- "Official-LNQ2023-Challenge-Submission": This is the trained model and the inference code used for the official challenge submission.
Performance on the LNQ2023 Test Set: Dice score=0.628, ASSD=5.8mm

## Using the model via Grand Challenge Website

You can also use the algorithms docker container hosted at https://grand-challenge.org/algorithms/mediastinallymphnodesegmentation/ which refers to the "Official-LNQ2023-Challenge-Submission".

## Use model weights with nnUNet

If you are used to the nnUNet framework you could also just use the nnUNetv2 model weigths and model plans which are all located at: https://gitlab.lrz.de/compai/MediastinalLymphNodeSegmentation/-/tree/main/lnq_segmentation/nnunet?ref_type=heads



# Training of own Model
If you want to train a model on your own data, you just need to install and use nnUNet and get the data which is all publicly available. The LNQ2023 challenge data will be published by the challenge host at some point in the future.




